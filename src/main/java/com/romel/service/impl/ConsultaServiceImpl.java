package com.romel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.romel.dao.IConsultaDAO;
import com.romel.dao.IConsultaExamenDAO;
import com.romel.dto.ConsultaListaExamenDTO;
import com.romel.model.Consulta;
import com.romel.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService {

	@Autowired
	private IConsultaDAO dao;
	
	@Autowired
	private IConsultaExamenDAO ceDAO;

	@Transactional
	@Override
	public Consulta registrar(ConsultaListaExamenDTO consultaDTO) {				
		//java 8
		//consultaDTO.getDetalleConsulta().forEach(x -> x.setConsulta(consulta));		
		//return dao.save(consultaDTO);
		Consulta cons = new Consulta();
		consultaDTO.getConsulta().getDetalleConsulta().forEach(x -> x.setConsulta(consultaDTO.getConsulta()));
		cons = dao.save(consultaDTO.getConsulta());
		
		consultaDTO.getLstExamen().forEach(e -> ceDAO.registrar(consultaDTO.getConsulta().getIdConsulta(), e.getIdExamen()));
		
		return cons;
	}

	@Override
	public void modificar(Consulta consulta) {
		dao.save(consulta);
	}

	@Override
	public void eliminar(int idConsulta) {
		dao.delete(idConsulta);
	}

	@Override
	public Consulta listarId(int idConsulta) {
		return dao.findOne(idConsulta);
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

}
