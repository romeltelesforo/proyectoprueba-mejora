package com.romel.service;

import java.util.List;

import com.romel.dto.ConsultaListaExamenDTO;
import com.romel.model.Consulta;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);

	void modificar(Consulta consulta);

	void eliminar(int idConsulta);

	Consulta listarId(int idConsulta);

	List<Consulta> listar();
}
