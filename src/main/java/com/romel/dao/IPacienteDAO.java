package com.romel.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.romel.model.Paciente;

@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, Integer>{

}
