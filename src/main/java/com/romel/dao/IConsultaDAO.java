package com.romel.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.romel.model.Consulta;

@Repository
public interface IConsultaDAO extends JpaRepository<Consulta, Integer>{

}
